# Game of Life - SFML

This is a C++ implementation of Conway's Game of Life. It utilizes SFML, a cross-platform media layer similar to SDL, for better cross-platform compatibility. I programmed this in a day back in 2016.

It's only been tested on macOS, although it should work on Linux and Windows without any changes. 

## Screenshot

![Game screenshot](/screenshot.png)
