/* Conway's Game of Life - Implementation in C++
 * Author: Abdulhadi Kocabas
 * Date: 6 Dec 2016
 */
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <sstream>

#include <chrono>
#include <thread>
#include <random>

#include <sys/types.h>
#include <sys/stat.h>


#include <SFML/Graphics.hpp>

// Resources helper class for macOS
#include "ResourcePath.hpp"

#define FIELD_WIDTH  400
#define FIELD_HEIGHT 200

using namespace std;

void setupShapeGrid();
void generateField();
void clearField();
void printField();
void saveGame();
void loadGame();
void copyTempArrayToField();
int getNeighbourCount(int x, int y);

int stepCycle();

//Screen dimension constants
const int SCREEN_WIDTH = 2560;
const int SCREEN_HEIGHT = 1440;
const int CELL_SIZE = 8;
int FIELD_OFFSET_TOP = 400; // Pseudo-constant, calculated once at start

char field[FIELD_HEIGHT][FIELD_WIDTH];	// Because it's defined at global scope it's default-initialized with 0 (Wouldn't be if it was in a function)

char temp_field[FIELD_HEIGHT][FIELD_WIDTH];	// Used during calculation of next generation

sf::RectangleShape rect_field[FIELD_HEIGHT][FIELD_WIDTH];

int generation = 0;
int alive = 0;

bool gamePaused = true;
bool isFullscreen = false;

int main(int argc, char* argv[]) {
    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Life", sf::Style::Default);
    sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
    
    // Just to note: http://fontvir.us/fonts/8bit
    // CC0, so no mention needed
    sf::Font coolFont;
    if(!coolFont.loadFromFile(resourcePath() + "8bit.ttf")) {
        cout << "Shiiite" << endl;
        return EXIT_FAILURE;
    }
    
    sf::Text titleText;
    titleText.setFont(coolFont);
    titleText.setString("Conway's Game Of Life");
    titleText.setCharacterSize(72);
    titleText.setFillColor(sf::Color::White);
    sf::FloatRect titleTextRect = titleText.getLocalBounds();
    titleText.setPosition(0, 0);    // Unnecessary, just to make it explicit
    
    sf::Text authorText;
    authorText.setFont(coolFont);
    authorText.setString("by hadik");
    authorText.setCharacterSize(72);
    authorText.setFillColor(sf::Color::White);
    sf::FloatRect authorTextRect = authorText.getLocalBounds();
    authorText.setPosition(SCREEN_WIDTH-authorTextRect.width, 0);   // Position text in the top right
    
    sf::Text generationAliveText;
    generationAliveText.setFont(coolFont);
    generationAliveText.setString("Generation: XX \t Alive: XX");
    generationAliveText.setCharacterSize(72);
    generationAliveText.setFillColor(sf::Color::White);
    sf::FloatRect genAliveTextRect = generationAliveText.getLocalBounds();
    generationAliveText.setPosition(0, titleTextRect.height);
    
    sf::Text howtoText;
    howtoText.setFont(coolFont);
    howtoText.setString("space to pause, c to clear, r to restart, q to quit");
    howtoText.setCharacterSize(55);
    howtoText.setFillColor(sf::Color::White);
    sf::FloatRect howtoTextRect = howtoText.getLocalBounds();
    howtoText.setPosition(SCREEN_WIDTH-howtoTextRect.width, authorTextRect.height+15);
    
    FIELD_OFFSET_TOP = titleTextRect.height + genAliveTextRect.height + 50; // No idea why this doesn't work directly, but let's just keep a hacky offset in
    
    generateField();
    
    setupShapeGrid();
    
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch (event.type)
            {
                    // window closed
                case sf::Event::Closed:
                    window.close();
                    break;
                    
                    // key pressed
                case sf::Event::KeyPressed:
                    switch(event.key.code) {
                        case sf::Keyboard::Escape:
                            window.close();
                            break;
                        case sf::Keyboard::Q:
                            window.close();
                            break;
                        case sf::Keyboard::R:
                            if(gamePaused) {
                                generateField();
                                generation = 0;
                                alive = 0;
                            }
                            break;
                        case sf::Keyboard::Space:
                            gamePaused = !gamePaused;
                            break;
                        case sf::Keyboard::S:
                            saveGame();
                            break;
                        case sf::Keyboard::L:
                            loadGame();
                            break;
                        default:
                            break;
                    }
                    break;
                case sf::Event::KeyReleased:
                    switch(event.key.code) {
                        case sf::Keyboard::C:
                            // Clear field
                            if(gamePaused) {
                                clearField();
                                generation = 0;
                                alive = 0;
                            }
                            break;
                        case sf::Keyboard::F:
                            //isFullscreen = !isFullscreen;
                            //window.create(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Life", (isFullscreen ? sf::Style::Fullscreen : sf::Style::Resize|sf::Style::Close));
                            break;
                        default:
                            break;
                    }
                    break;
                case sf::Event::MouseButtonPressed:
                    if(gamePaused) {
                        // get the local mouse position (relative to a window)
                        sf::Vector2i localPosition = sf::Mouse::getPosition(window); // window is a sf::Window
                        stringstream ss;
                        ss << "MouseClicked: PosX - " << localPosition.x << " PosY - " << localPosition.y;
                        //cout << ss.str() << endl;
                        if(localPosition.y > FIELD_OFFSET_TOP) {
                            int column = localPosition.x / CELL_SIZE;
                            int row = (localPosition.y - FIELD_OFFSET_TOP) / CELL_SIZE;
                            cout << "Mouseclick on  cell at [" << row << "][" << column <<"]" << endl;
                            if(field[row][column] > 0) {
                                field[row][column] = 0;
                            }
                            else {
                                field[row][column] = 1;
                            }
                        }
                    }
                case sf::Event::MouseMoved:
                    /*if(sf::Mouse::isButtonPressed()) {
                        
                    }*/
                    break;
                default:
                    break;
            }
        }
        
        window.clear();
        
        window.draw(titleText);
        window.draw(authorText);
        window.draw(generationAliveText);
        window.draw(howtoText);

        for(int i = 0; i < FIELD_HEIGHT; i++) {
            for(int j = 0; j < FIELD_WIDTH; j++) {
                if(field[i][j] > 0) {
                    window.draw(rect_field[i][j]);
                }
            }
        }
        
        window.display();
        
        stringstream ss;
        ss << "Generation: " << generation << "\tAlive: " << alive;
        generationAliveText.setString(ss.str());
        
        if(!gamePaused) {
            // Reset our temporary field to all 0, C++-way would be std::fill
            memset(temp_field, 0, sizeof(temp_field));
            
            // Progress!
            stepCycle();
            
            // Reset our field to prevent any bugs from hiding
            memset(field, 0, sizeof(field));
            
            // I wrote my own function to copy the temporary array to our field because it was literally faster than looking this up
            // C++ function for reference on how to do it
            // copy(&temp_field[0][0], &temp_field[0][0]+FIELD_HEIGHT*FIELD_WIDTH, &field[0][0]);	// This C++ function copies our temporary field to our real field before displaying it. C-way would be memcpy
            copyTempArrayToField();
            
            // Increment our generation counter
            generation += 1;
        }
        
        // We don't want this over too fast!
        this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}

void setupShapeGrid() {
    for(int i = 0; i < FIELD_HEIGHT; i++) {
        for(int j = 0; j < FIELD_WIDTH; j++) {
            rect_field[i][j] = sf::RectangleShape(sf::Vector2f(CELL_SIZE, CELL_SIZE));
            rect_field[i][j].setFillColor(sf::Color::White);
            rect_field[i][j].setPosition(j*CELL_SIZE, i*CELL_SIZE+FIELD_OFFSET_TOP);
        }
    }
}

int stepCycle() {
    alive = 0;
    
    for(int i = 0; i < FIELD_HEIGHT; i++) {
        for(int j = 0; j < FIELD_WIDTH; j++) {
            int neighbourCells = getNeighbourCount(i, j);
            
            if(neighbourCells == 3) {
                temp_field[i][j] = 1;
                alive += 1;
            }
            else if(neighbourCells == 2) {
                if(field[i][j] == 1) {
                    temp_field[i][j] = 1;
                    alive += 1;
                }
            }
            else if(neighbourCells < 2) {
                temp_field[i][j] = 0;
            }
            else if(neighbourCells > 3) {
                temp_field[i][j] = 0;
            }
        }
    }
    
    return alive;
}

void generateField() {
    srand(time(0));

    // Fill field randomly
    // Now with real C++11 randomness! (Not yet)
    
    for(int i = 0; i < FIELD_HEIGHT; i++) {
        for(int j = 0; j < FIELD_WIDTH; j++) {
            int num = rand()%9;
            if(num < 4) {
                field[i][j] = 1;
            }
            else {
                field[i][j] = 0;
            }
        }
    }
}

void clearField() {
    memset(temp_field, 0, sizeof(temp_field));
    memset(field, 0, sizeof(field));
}

void printField() {
    system("clear");
    
    cout << "--------------------------------------------------------------------" << endl;
    cout << "|           Conway's Game of Life     |     by hadik               |" << endl;
    cout << "|         Gen: " << generation << "                      |     Alive: " << alive << "               |" << endl;
    cout << "--------------------------------------------------------------------" << endl;
    
    for(int i = 0; i < FIELD_HEIGHT; i++) {
        for(int j = 0; j < FIELD_WIDTH; j++) {
            if(field[i][j] == 0) {
                cout << " -";
            }
            else {
                cout << " *";
            }
        }
        
        cout << endl;
    }
}

int getNeighbourCount(int i, int j) {	// Where i is row and j is column
    if(i >= FIELD_HEIGHT) { return 0; }
    if(j >= FIELD_WIDTH) { return 0; }
    
    int result = 0;
    
    if(i > 0 && j > 0) {
        result += field[i-1][j-1];	// Top left
    }
    
    if(j > 0) {
        result += field[i][j-1];	// Center left
    }
    
    if(i > 0 && j < (FIELD_WIDTH - 1)) {
        result += field[i-1][j+1];	// Top right
    }
    
    if(i > 0) {
        result += field[i-1][j];	// Top center
    }
    
    if(j < (FIELD_WIDTH - 1)) {
        result += field[i][j+1];	// Center right
    }
    
    if(i < (FIELD_HEIGHT -1) && j > 0) {
        result += field[i+1][j-1];	// Bottom left
    }
    
    if(i < (FIELD_HEIGHT - 1)) {
        result += field[i+1][j];	// Bottom center
    }
    
    if(i < (FIELD_HEIGHT - 1) && j < (FIELD_WIDTH -1)) {
        result += field[i+1][j+1];	// Bottom right
        
    }
    
    return result;
}

void copyTempArrayToField() {
    
    for(int i = 0; i < FIELD_HEIGHT; i++) {
        for(int j = 0; j < FIELD_WIDTH; j++) {
            field[i][j] = temp_field[i][j];
        }
    }
}

void saveGame() {
    
}

void loadGame() {
    
}

bool directoryExists(string pathname) {
    /*struct stat info;
    
    if( stat( pathname, &info ) != 0 )
        printf( "cannot access %s\n", pathname );
    else if( info.st_mode & S_IFDIR )  // S_ISDIR() doesn't exist on my windows
        printf( "%s is a directory\n", pathname );
    else
        printf( "%s is no directory\n", pathname );*/
    return false;
}
